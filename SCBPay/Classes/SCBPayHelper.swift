//
//  SCBPayHelper.swift
//  SCBPay
//
//  Created by MECHIN on 5/10/18.
//

import Foundation

public struct SCBScheme {
    public static let PAY_PATH = "scbeasy://scan/camera"
    public static let SCBEASY_PATH = "https://itunes.apple.com/th/app/scb-easy/id568388474"
}

public class SCBPayHelper: NSObject {
    public static let sharedInstance = SCBPayHelper()
    public static func pay() {
        if let url = URL(string: SCBScheme.PAY_PATH),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }else if let scbeasyStoreUrl = URL(string: SCBScheme.SCBEASY_PATH) {
            UIApplication.shared.openURL(scbeasyStoreUrl)
        }
    }
}
