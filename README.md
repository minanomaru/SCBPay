# SCBPay

[![CI Status](https://img.shields.io/travis/mechin.maru@gmail.com/SCBPay.svg?style=flat)](https://travis-ci.org/mechin.maru@gmail.com/SCBPay)
[![Version](https://img.shields.io/cocoapods/v/SCBPay.svg?style=flat)](https://cocoapods.org/pods/SCBPay)
[![License](https://img.shields.io/cocoapods/l/SCBPay.svg?style=flat)](https://cocoapods.org/pods/SCBPay)
[![Platform](https://img.shields.io/cocoapods/p/SCBPay.svg?style=flat)](https://cocoapods.org/pods/SCBPay)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SCBPay is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SCBPay'
```

## Author

mechin.maru@gmail.com, mechin.maru@gmail.com

## License

SCBPay is available under the MIT license. See the LICENSE file for more info.
