//
//  ViewController.swift
//  SCBPay
//
//  Created by mechin.maru@gmail.com on 05/09/2018.
//  Copyright (c) 2018 mechin.maru@gmail.com. All rights reserved.
//

import UIKit
import SCBPay
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func payButtonClicked(_ sender: Any) {
        SCBPayHelper.pay()
    }
}

